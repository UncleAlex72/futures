package uk.co.unclealex.futures

import cats.{CoflatMap, Monad, MonadError}
import cats.data.{EitherT, OptionT}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

/** Syntactic Sugar for monad transformers and futures.
  */
//noinspection ScalaDocUnknownParameter
trait Futures {

  /** The combination of future and option
    * @tparam T The type being wrapped.
    */
  type FutureOption[T] = OptionT[Future, T]

  /** The combination of future and either.
    * @tparam L The left type being wrapped.
    * @tparam R The right type being wrapped.
    */
  type FutureEither[L, R] = EitherT[Future, L, R]

  /** Re-export an implicit for a `Functor[Future[_]]` so that no extra cats imports are required.
    * @param ec An execution context.
    * @return Cats' standard instances for futures.
    */
  implicit def futureInstances(implicit
      ec: ExecutionContext
  ): MonadError[Future, Throwable] & CoflatMap[Future] & Monad[Future] =
    cats.instances.future.catsStdInstancesForFuture

  /** Convert an option wrapped in a future into a FutureOption.
    * @param v the value to convert.
    * @tparam T The underlying wrapped type.
    * @return A FutureOption
    */
  def fo[T](v: Future[Option[T]]): FutureOption[T] = OptionT(v)

  /** Convert a future into a FutureOption.
    * @param v the value to convert.
    * @tparam T The underlying wrapped type.
    * @return A FutureOption
    */
  def fo[T](v: Future[T])(implicit ec: ExecutionContext): FutureOption[T] =
    OptionT(v.map(Option(_)))

  /** Convert an option into a FutureOption.
    * @param v the value to convert.
    * @tparam T The underlying wrapped type.
    * @return A FutureOption
    */
  def fo[T](v: Option[T]): FutureOption[T] = OptionT(Future.successful(v))

  /** Convert an object into a FutureOption.
    * @param v the value to convert.
    * @tparam T The type of v.
    * @return A FutureOption
    */
  def fo[T](v: T): FutureOption[T] = OptionT(Future.successful(Option(v)))

  /** Unwrap a `FutureOption[T]` back into a `Future[Option[T]]`. This is to allow, for example, for blocks to be
    * wrapped and not required being assigned to a variable.
    *
    * {{{
    *   ToFuture {
    *     for {
    *       v <- fo(something)
    *     } yield {
    *       v
    *     }
    *   }
    * }}}
    * @param v The value to unwrap.
    * @tparam T The underlying wrapped type.
    * @return
    */
  def ToFuture[T](v: FutureOption[T]): Future[Option[T]] = v.value

  /** Convert an either wrapped in a future into a FutureEither.
    * @param v the value to convert.
    * @tparam L The underlying wrapped left type.
    * @tparam R The underlying wrapped right type.
    * @return A FutureOption
    */
  def fe[L, R](v: Future[Either[L, R]]): FutureEither[L, R] = EitherT(v)

  /** Convert a future into a FutureEither.
    * @param v the value to convert.
    * @tparam L The underlying wrapped left type.
    * @tparam R The underlying wrapped right type.
    * @return A FutureOption
    */
  def fe[L, R](
      v: Future[R]
  )(implicit ec: ExecutionContext): FutureEither[L, R] = {
    val right: Future[Either[L, R]] = v.map(Right(_))
    EitherT(right)
  }

  /** Convert an either into a FutureEither.
    * @param v the value to convert.
    * @tparam L The underlying wrapped left type.
    * @tparam R The underlying wrapped right type.
    * @return A FutureOption
    */
  def fe[L, R](v: Either[L, R]): FutureEither[L, R] = EitherT(
    Future.successful(v)
  )

  /** Convert an object into a FutureEither.
    * @param v the value to convert.
    * @tparam L The underlying wrapped left type.
    * @tparam R The underlying wrapped right type.
    * @return A FutureOption
    */
  def fe[L, R](v: R): FutureEither[L, R] = {
    val right: Future[Either[L, R]] = Future.successful(Right(v))
    EitherT(right)
  }

  /** Unwrap a `FutureEither[L, R]` back into a `Future[Either[L, R]]`. This is to allow, for example, for blocks to be
    * wrapped and not required being assigned to a variable.
    *
    * {{{
    *   ToFuture {
    *     for {
    *       v <- fe(something)
    *     } yield {
    *       v
    *     }
    *   }
    * }}}
    * @param v The value to unwrap.
    * @tparam L The underlying wrapped left type.
    * @tparam R The underlying wrapped right type.
    * @return
    */
  def ToFuture[L, R](v: FutureEither[L, R]): Future[Either[L, R]] = v.value

  implicit class FutureExtensions[A](future: Future[A]) {

    def close(
        closeBlock: () => Unit
    )(implicit ec: ExecutionContext): Future[A] = {
      future.andThen {
        case Success(value) =>
          closeBlock()
          value
        case Failure(ex) =>
          closeBlock()
          throw ex
      }
    }
  }
}

object Futures extends Futures
