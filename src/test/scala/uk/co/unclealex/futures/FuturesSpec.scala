package uk.co.unclealex.futures

import org.scalatest.matchers.should
import org.scalatest.wordspec.AsyncWordSpec
import uk.co.unclealex.futures.Futures._

import scala.concurrent.Future

class FuturesSpec extends AsyncWordSpec with should.Matchers {

  "A future option" should {
    "be able to convert a Some wrapped in a future" in {
      val eventualMaybeResult: FutureOption[Int] = for {
        r <- fo(Future.successful(Some(1)))
      } yield {
        r
      }
      eventualMaybeResult.value.map { maybeResult =>
        maybeResult should ===(Some(1))
      }
    }

    "be able to convert a None wrapped in a future" in {
      val eventualMaybeResult: FutureOption[Int] = for {
        r <- fo(Future.successful(Option.empty[Int]))
      } yield {
        r
      }
      eventualMaybeResult.value.map { maybeResult =>
        maybeResult should ===(None)
      }
    }

    "be able to convert a Some" in {
      val eventualMaybeResult: FutureOption[Int] = for {
        r <- fo(Some(1))
      } yield {
        r
      }
      eventualMaybeResult.value.map { maybeResult =>
        maybeResult should ===(Some(1))
      }
    }

    "be able to convert a None" in {
      val eventualMaybeResult: FutureOption[Int] = for {
        r <- fo(Option.empty[Int])
      } yield {
        r
      }
      eventualMaybeResult.value.map { maybeResult =>
        maybeResult should ===(None)
      }
    }

    "be able to convert a future" in {
      val eventualMaybeResult: FutureOption[Int] = for {
        r <- fo(Future.successful(1))
      } yield {
        r
      }
      eventualMaybeResult.value.map { maybeResult =>
        maybeResult should ===(Some(1))
      }
    }

    "be able to convert an object" in {
      val eventualMaybeResult: FutureOption[Int] = for {
        r <- fo(1)
      } yield {
        r
      }
      eventualMaybeResult.value.map { maybeResult =>
        maybeResult should ===(Some(1))
      }
    }

    "be able to convert back to a future" in {
      val eventualMaybeResult: Future[Option[Int]] = ToFuture {
        for {
          r <- fo(1)
        } yield {
          r
        }
      }
      eventualMaybeResult.map { maybeResult =>
        maybeResult should ===(Some(1))
      }
    }
  }

  "A future either" should {
    "be able to convert a Right wrapped in a future" in {
      val eventualMaybeResult: FutureEither[String, Int] = for {
        r <- fe(Future.successful(Right(1)))
      } yield {
        r
      }
      eventualMaybeResult.value.map { maybeResult =>
        maybeResult should ===(Right(1))
      }
    }

    "be able to convert a Left wrapped in a future" in {
      val eventualMaybeResult: FutureEither[String, Int] = for {
        r <- fe(Future.successful(Left[String, Int]("xyz")))
      } yield {
        r
      }
      eventualMaybeResult.value.map { maybeResult =>
        maybeResult should ===(Left[String, Int]("xyz"))
      }
    }

    "be able to convert a Right" in {
      val eventualMaybeResult: FutureEither[String, Int] = for {
        r <- fe(Right(1))
      } yield {
        r
      }
      eventualMaybeResult.value.map { maybeResult =>
        maybeResult should ===(Right(1))
      }
    }

    "be able to convert a Left" in {
      val eventualMaybeResult: FutureEither[String, Int] = for {
        r <- fe(Left[String, Int]("xyz"))
      } yield {
        r
      }
      eventualMaybeResult.value.map { maybeResult =>
        maybeResult should ===(Left("xyz"))
      }
    }

    "be able to convert a future" in {
      val eventualMaybeResult: FutureEither[String, Int] = for {
        r <- fe(Future.successful(1))
      } yield {
        r
      }
      eventualMaybeResult.value.map { maybeResult =>
        maybeResult should ===(Right(1))
      }
    }

    "be able to convert an object" in {
      val eventualMaybeResult: FutureEither[String, Int] = for {
        r <- fe(1)
      } yield {
        r
      }
      eventualMaybeResult.value.map { maybeResult =>
        maybeResult should ===(Right(1))
      }
    }

    "be able to convert back to a future" in {
      val eventualMaybeResult: Future[Either[String, Int]] = ToFuture {
        for {
          r <- fe(Right(1))
        } yield {
          r
        }
      }
      eventualMaybeResult.map { maybeResult =>
        maybeResult should ===(Right(1))
      }
    }

  }

  "The close extension method" should {
    "close a resource on success" in {
      val closer = new Closer()
      Future(1).close(() => closer.close()).map { result =>
        result should ===(1)
        closer.closed should ===(true)
      }
    }
    "close a resource on failure" in {
      val closer = new Closer()
      Future[Int](throw new IllegalStateException("Hello"))
        .close(() => closer.close())
        .recover { case _ =>
          1
        }
        .map { result =>
          result should ===(1)
          closer.closed should ===(true)
        }
    }
  }

  class Closer(var closed: Boolean = false) {
    def close(): Unit = {
      closed = true
    }
  }
}
